# OpenCV-based Web Camera Processor

Transforms the video stream from a webcam to beautifulize your box in online meetings.

### Supported transformations:

* crop
  * zoom
  * pan
  * the current transformation is presisted in a `./user-transform.pickle` file
* PNG image overlay
  * the [overlay image](config.py.example#L22) size should equal to the [output image size](config.py.example#L11)
  * partial opacity is [not supported yet](main.py#L73) (waiting for your merge request)

## How to use

0. tested on Ubuntu 20.04 only (running on other systems may require your changes)
1. examine and run [init-ubuntu.sh](init-ubuntu.sh)
2. configure your webcam device in [config.py](config.py.example#L3)
3. examine and run [main.py](main.py)
4. configure your meeting software to use a new virtual camera
5. use `+/-` and `arrow keys` to crop the mess around you
6. use `Enter` to replace yourself with the Pickle Rick
7. use `Esc` to exit