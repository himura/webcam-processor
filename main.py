#!/usr/bin/python3

import os
import cv2
import time
import pickle
import pyfakewebcam
from copy import deepcopy
from config import config

def process(frame):
    target_size = (round(config['virtual_cam']['w'] * config['transform']['scale_factor']),
                   round(config['virtual_cam']['h'] * config['transform']['scale_factor']))
    frame = cv2.resize(frame, target_size)
    frame = frame[config['transform']['shift']['y'] : config['virtual_cam']['h'] + config['transform']['shift']['y'],
                  config['transform']['shift']['x'] : config['virtual_cam']['w'] + config['transform']['shift']['x']]

    if 'overlay' in config and config['overlay']['show']:
        background = cv2.bitwise_and(frame, frame, mask=mask_inv)
        foreground = cv2.bitwise_and(overlay_img, overlay_img, mask=mask)
        frame = cv2.add(background, foreground)
    return frame


shift_step_y = 10
shift_step_x = -shift_step_y if config['real_cam']['mirror_preview'] else shift_step_y

keys = { 27: 'esc', 81: 'left', 82: 'up', 83: 'right', 84: 'down', 171: '+', 173: '-', 13: 'enter', 141: 'enter' }

def control(key_code):
    global prev_transform
    if key_code not in keys.keys():
        print('key:', key_code)
        return True
    key = keys[key_code]
    prev_transform = deepcopy(config['transform'])
    if key == 'esc':
        return False
    elif key == 'left':
        config['transform']['shift']['x'] -= shift_step_x
    elif key == 'up':
        config['transform']['shift']['y'] -= shift_step_y
    elif key == 'right':
        config['transform']['shift']['x'] += shift_step_x
    elif key == 'down':
        config['transform']['shift']['y'] += shift_step_y
    elif key == '+':
        config['transform']['scale_factor'] += 0.01
    elif key == '-':
        config['transform']['scale_factor'] -= 0.01
    elif key == 'enter' and 'overlay' in config:
        config['overlay']['show'] = not config['overlay']['show']
        return True
    print(config['transform'])
    return True

prev_transform = deepcopy(config['transform'])
if os.path.isfile('user-transform.pickle'):
    config['transform'] = pickle.load(open('user-transform.pickle', 'rb'))

if not os.path.exists(config['real_cam']['dev']):
    print(config['real_cam']['dev'], 'not found. Check ./config.py.')

if not os.path.exists(config['virtual_cam']['dev']):
    print(config['virtual_cam']['dev'], 'not found. Adding a v4l2loopback camera...')
    requested_video_nr = int(config['virtual_cam']['dev'][len('/dev/video'):])
    os.system(f'sudo modprobe v4l2loopback video_nr={requested_video_nr}')

if 'overlay' in config:
    overlay_img = cv2.imread(config['overlay']['img_path'], cv2.IMREAD_UNCHANGED)
    overlay_img, alpha_channel = overlay_img[:,:,:3], overlay_img[:,:,3]
    _, mask = cv2.threshold(alpha_channel, config['overlay']['mask_threshold'], 255, cv2.THRESH_BINARY)
    mask_inv = cv2.bitwise_not(mask)

r_cam = cv2.VideoCapture(config['real_cam']['dev'])
r_cam.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter.fourcc('M', 'J', 'P', 'G'))
r_cam.set(cv2.CAP_PROP_FRAME_WIDTH, config['real_cam']['w'])
r_cam.set(cv2.CAP_PROP_FRAME_HEIGHT, config['real_cam']['h'])
r_cam.set(cv2.CAP_PROP_FPS, config['real_cam']['FPS'])
if config['real_cam']['focus_inf']:
    r_cam.set(cv2.CAP_PROP_AUTOFOCUS, 0)
    r_cam.set(cv2.CAP_PROP_FOCUS, 0)
if config['real_cam']['white_balance']:
    r_cam.set(cv2.CAP_PROP_AUTO_WB, 0)
    r_cam.set(cv2.CAP_PROP_WB_TEMPERATURE, config['real_cam']['white_balance'])

v_cam = pyfakewebcam.FakeWebcam(config['virtual_cam']['dev'], config['virtual_cam']['w'], config['virtual_cam']['h'])
while True:
    success, frame = r_cam.read()
    if not success:
        print("cv2.VideoCapture().read() failed")
        continue
    frame = process(frame)
    try:
        v_cam.schedule_frame(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
    except Exception as e:
        print(f'{type(e).__name__}: {e}')
        config['transform'] = prev_transform
    else:
        pickle.dump(config['transform'], open('user-transform.pickle', 'wb'))
        if config['real_cam']['mirror_preview']:
            frame = cv2.flip(frame, 1)
        cv2.imshow('v_cam', frame)
    key = cv2.waitKey(config['virtual_cam']['frame_delay_ms'])
    if key > 0 and not control(key):
        break

r_cam.release()
cv2.destroyAllWindows()
